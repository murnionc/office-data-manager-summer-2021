from flask_restplus import Resource
from flask import current_app

from officedatamanager.main.utils.swagger import documented_endpoint
from officedatamanager.main.docs.specs.offices_spec import GeoidSpec, OfficeIDSpec, OfficeUpdateSpec
from officedatamanager.main.lib.offices_logic import geoids_search, office_ids_search, update_office

offices = GeoidSpec.api


@offices.route('/geoids')
class OfficesByGeoid(Resource):

    @documented_endpoint(GeoidSpec)
    def post(self, geoids):
        return geoids_search(current_app, geoids)


@offices.route('/office_ids')
class OfficesByID(Resource):

    @documented_endpoint(OfficeIDSpec)
    def post(self, office_ids):
        return office_ids_search(current_app, office_ids)


@offices.route('/update')
class OfficesUpdate(Resource):

    @documented_endpoint(OfficeUpdateSpec)
    def post(self, office_id, office):
        if not office_id:
            return "No office_id provided in query arguments.", 400
        else:
            try:
                update_office(current_app, office_id, office)
            except Exception as e:
                return e.args[0], 400
            return "Update complete to officeID " + office_id, 200

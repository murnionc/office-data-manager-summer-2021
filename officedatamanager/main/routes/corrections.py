from flask_restplus import Resource
from flask import current_app
from jsonschema import ValidationError

from officedatamanager.main.utils.swagger import documented_endpoint
from officedatamanager.main.docs.specs.corrections_spec import (
    CorrectionIDSpecPreview, CorrectionIDSpecFull, CorrectionSpecCreate, CorrectionSpecUpdateStatus)
from officedatamanager.main.lib.corrections_logic import (
    correction_id_search_detail, correction_search, correction_create, correction_update_status)

corrections = CorrectionIDSpecPreview.api


@corrections.route('/search')
class CorrectionGetPreview(Resource):

    @documented_endpoint(CorrectionIDSpecPreview)
    def get(self, status):
        data = correction_search(current_app, status)
        print(data)
        return data


@corrections.route('/searchDetail')
class CorrectionGetFull(Resource):

    @documented_endpoint(CorrectionIDSpecFull)
    def get(self, correction_id):
        try:
            data = correction_id_search_detail(current_app, correction_id)
        except Exception:
            return "No document found with correctionID " + correction_id, 400
        print(data)
        return data


@corrections.route('/create')
class CorrectionCreate(Resource):

    @documented_endpoint(CorrectionSpecCreate)
    def post(self, correction):
        try:
            data = correction_create(current_app, correction)
        except ValidationError as e:
            error_string = f'Validation error on input: {e.message}'
            print(error_string)
            return error_string, 400
        except Exception as e:
            print(e)
            return e, 400
        print(data)
        return data


@corrections.route('/updateStatus')
class CorrectionUpdate(Resource):

    @documented_endpoint(CorrectionSpecUpdateStatus)
    def post(self, correction_id, status, verifier):
        try:
            correction_update_status(current_app, correction_id, status, verifier)
        except Exception as e:
            return e.args[0], 400
        return "Update complete to documentID " + correction_id, 200

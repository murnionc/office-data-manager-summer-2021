from flask_restplus import Resource
from flask import current_app

from officedatamanager.main.utils.swagger import documented_endpoint
from officedatamanager.main.docs.specs.meetings_spec import GeoidSpec, MeetingUpdateSpec
from officedatamanager.main.lib.meetings_logic import geoids_search, update_meeting

meetings = GeoidSpec.api


@meetings.route('/geoids')
class MeetingsByGeoid(Resource):

    @documented_endpoint(GeoidSpec)
    def post(self, geoids):
        return geoids_search(current_app, geoids)


@meetings.route('/update')
class MeetingsUpdate(Resource):

    @documented_endpoint(MeetingUpdateSpec)
    def post(self, meeting_id, meeting):
        if not meeting_id:
            return "No meeting_id provided in query arguments.", 400
        else:
            try:
                update_meeting(current_app, meeting_id, meeting)
            except Exception as e:
                return e.args[0], 400
            return "Update complete to officeID " + meeting_id, 200

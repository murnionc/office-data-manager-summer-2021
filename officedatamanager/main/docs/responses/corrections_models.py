from flask_restplus import Namespace, fields

from .fields import (
    changed_field_names,
    correction_type_field,
    geoid_field,
    doc_id_field,
    correction_id_field,
    email_field,
    user_id_field,
    create_datetime_field,
    update_datetime_field,
    changed_field_name,
    old_change_field,
    new_change_field,
    correction_status_field,
    correction_source_field
)

api = Namespace('Corrections', description='Fetch data about corrections documents')

corrections_preview_model = api.model('Corrections Preview', {
    'corrections': fields.List(fields.Nested(api.model('Single Correction Preview', {
        'type': correction_type_field,
        'docId': doc_id_field,
        'correctionId': correction_id_field,
        'create_timestamp': create_datetime_field,
        'update_timestamp': update_datetime_field,
        'changedFields': changed_field_names,
        'status': correction_status_field
    })))
})

corrections_full_model = api.model('Corrections Full', {
    'correction': fields.List(fields.Nested(api.model('Single Correction Full', {
        'correctionId': correction_id_field,
        'changedFields': changed_field_names,
        'changes': fields.List(fields.Nested(api.model('Change', {
            'fieldname': changed_field_name,
            'new': new_change_field,
            'old': old_change_field
        }))),
        'corrector': fields.Nested(api.model('Corrector', {
            'email': email_field,
            'uid': user_id_field
        })),
        'geoid': geoid_field,
        'docId': doc_id_field,
        'create_timestamp': create_datetime_field,
        'update_timestamp': update_datetime_field,
        'type': correction_type_field,
        'source': correction_source_field,
        'status': correction_status_field,
        'verifier': fields.Nested(api.model('Verifier', {
            'email': email_field,
            'uid': user_id_field
        }))
    })))
})

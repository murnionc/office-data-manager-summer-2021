from .parameters import geoids_parameter, meeting_id_arg_parameter, meeting_update_parameter
from ..responses.meetings_models import api, meetings_model


class GeoidSpec:
    api = api
    params = [geoids_parameter]
    responses = {
        200: meetings_model
    }


class MeetingUpdateSpec:
    api = api
    params = [meeting_id_arg_parameter, meeting_update_parameter]
    responses = {
        200: None
    }

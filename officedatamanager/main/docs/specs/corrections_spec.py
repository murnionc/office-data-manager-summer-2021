from .parameters import (
    correction_id_parameter,
    correction_status_parameter,
    correction_create_parameter,
    correction_verifier_parameter
)
from ..responses.corrections_models import (
    api,
    corrections_preview_model,
    corrections_full_model,
)


class CorrectionIDSpecPreview:
    api = api
    params = [correction_status_parameter]
    responses = {
        200: corrections_preview_model
    }


class CorrectionIDSpecFull:
    api = api
    params = [correction_id_parameter]
    responses = {
        200: corrections_full_model
    }


class CorrectionSpecCreate:
    api = api
    params = [correction_create_parameter]
    responses = {
        200: corrections_full_model
    }


class CorrectionSpecUpdateStatus:
    api = api
    params = [correction_id_parameter, correction_status_parameter, correction_verifier_parameter]
    responses = {
        200: None
    }

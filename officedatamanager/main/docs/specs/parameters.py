geoids_parameter = ('geoids', {
    'type': list,
    'help': 'List of geoids to be searched',
    'location': 'json'
})

office_id_parameter = ('office_ids', {
    'type': list,
    'help': 'List of office ids to be searched',
    'location': 'json'
})

office_id_arg_parameter = ('office_id', {
    'type': str,
    'help': 'The ID of the office to update',
    'location': 'args'
})

office_update_parameter = ('office', {
    'type': dict,
    'help': 'Data to update an office record from',
    'location': 'json'
})

correction_id_parameter = ('correction_id', {
    'type': str,
    'help': 'Correction id to be searched',
    'location': 'args'
})

correction_status_parameter = ('status', {
    'type': str,
    'help': 'Optional - The status of the correction: open, accepted, rejected',
    'location': 'args'
})

correction_create_parameter = ('correction', {
    'type': dict,
    'help': 'Data to create a correction record from',
    'location': 'json'
})

correction_verifier_parameter = ('verifier', {
    'type': dict,
    'help': 'The verifier that either accepted or rejected this correction',
    'location': 'json'
})

meeting_id_arg_parameter = ('meeting_id', {
    'type': str,
    'help': 'The ID of the meeting to update',
    'location': 'args'
})

meeting_update_parameter = ('meeting', {
    'type': dict,
    'help': 'Data to update a meeting record from',
    'location': 'json'
})

from .parameters import (
    geoids_parameter,
    office_id_parameter,
    office_update_parameter,
    office_id_arg_parameter,
)
from ..responses.offices_models import (
    api,
    offices_preview_model,
    offices_full_model
)


class GeoidSpec:
    api = api
    params = [geoids_parameter]
    responses = {
        200: offices_preview_model
    }


class OfficeIDSpec:
    api = api
    params = [office_id_parameter]
    responses = {
        200: offices_full_model
    }


class OfficeUpdateSpec:
    api = api
    params = [office_id_arg_parameter, office_update_parameter]
    responses = {
        200: None
    }

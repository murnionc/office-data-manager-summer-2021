correction_schema = {
    "type": "object",
    "properties": {
        "changedFields": {
            "type": "array",
            "items": {
                "type": "string"
            },
            "minItems": 1,
            "uniqueItems": True
        },
        "changes":  {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "fieldname": {"type": "string"},
                    "new": {"type": "string"},
                    "old": {"type": "string"}
                },
                "additionalProperties": False,
                "required": ["fieldname", "new", "old"]
            },
            "minItems": 1,
        },
        "corrector": {
            "type": "object",
            "properties": {
                "email": {"type": "string"},
                "uid": {"type": "string"}
            },
            "additionalProperties": False,
            "required": ["email", "uid"]
        },
        "create_timestamp": {"type": "number"},
        "geoid": {"type": "string"},
        "docId": {"type": "string"},
        "source": {"type": "string"},
        "status": {"type": "string"},
        "type": {"type": "string"},
        "update_timestamp": {"type": "number"},
        "verifier": {
            "type": "object",
            "properties": {
                "email": {"type": "string"},
                "uid": {"type": "string"}
            },
            "additionalProperties": False,
            "required": ["email", "uid"]
        }
    },
    "additionalProperties": False,
    "required": ["changedFields", "changes", "corrector", "create_timestamp", "geoid", "docId", "status", "type"]
}

verifier_schema = {
    "type": "object",
    "properties": {
        "email":  {"type": "string"},
        "uid": {"type": "string"}
    },
    "additionalProperties": False,
    "required": ["uid"]
}

meeting_schema = {
    "type": "object",
    "properties": {
        "geoid": {"type": "string"},
        "location.address.city": {"type": "string"},
        "location.address.state": {"type": "string"},
        "location.address.street": {"type": "string"},
        "location.address.zip": {"type": "string"},
        "location.virtual": {"type": "string"},
        "schedule": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "day": {"type": "number"},
                    "firstDate": {"type": "number"},
                    "frequency": {"type": "number"},
                    "time": {
                        "type": "object",
                        "properties": {
                            "hour": {"type": "number"},
                            "minute": {"type": "number"}
                        },
                        "additionalProperties": False,
                        "required": ["hour", "minute"]
                    },
                    "timezone": {"type": "number"},
                    "week": {"type": "number"}
                },
                "additionalProperties": False,
                "required": ["day", "firstDate", "frequency", "time", "timezone", "week"]
            }
        },
        "website": {"type": "string"}
    },
    "additionalProperties": False
}

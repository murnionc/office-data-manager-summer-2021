from jsonschema import validate
from firebase_admin import firestore
from .correction_schema import correction_schema, verifier_schema


def correction_search(app, status):
    data = []
    # Collection for corrections
    col_ref = app.officedb.collection('corrections')
    # If we are filtering by status, use a where clause
    if status:
        docs = [doc for doc in col_ref.where('status', '==', status).stream()]
    else:
        # Otherwise just return all documents
        docs = [doc.get() for doc in col_ref.list_documents()]

    # Add in the doc ids to data
    for doc in docs:
        doc_with_id = doc.to_dict()
        doc_with_id['correctionId'] = doc.id
        data.append(doc_with_id)

    return {'corrections': data}


def correction_id_search_detail(app, correction_id):
    # Collection for corrections
    col_ref = app.officedb.collection('corrections')

    # Get correction document matching the given correction_id
    doc = col_ref.document(correction_id).get()
    doc_with_id = doc.to_dict()
    doc_with_id['correctionId'] = doc.id

    return {
        'correction': doc_with_id
    }


def correction_create(app, data):
    # Validate the input JSON data against JSON schema
    validate(instance=data, schema=correction_schema)

    # Collection for corrections
    col_ref = app.officedb.collection('corrections')

    # Create the document and return what was created
    doc = col_ref.add(data)
    doc = doc[1].get()
    doc_with_id = doc.to_dict()
    doc_with_id['correctionId'] = doc.id
    return {"correction": doc_with_id}


def correction_update_status(app, correction_id, status, verifier):
    if status.lower() not in ['open', 'rejected', 'accepted']:
        raise ValueError("Status " + status + " not a valid status. Try open, rejected, or accepted.")
    else:
        fields = {"status": status, "update_timestamp": firestore.SERVER_TIMESTAMP, "verifier": verifier}
        if verifier:
            validate(instance=verifier, schema=verifier_schema)
            fields["verifier"] = verifier
        col_ref = app.officedb.collection('corrections')
        col_ref.document(correction_id).update(fields)

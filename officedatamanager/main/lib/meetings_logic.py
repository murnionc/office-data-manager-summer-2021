from jsonschema import validate
from .meeting_schema import meeting_schema


def geoids_search(app, geoids):
    data = []

    # Collection for meeting info filtered by geoid
    col_ref = app.officedb.collection('meetings')

    for geoid in geoids:
        # Fetch all relevant meetings for geoid
        data.append([doc.to_dict() for doc in col_ref.where('geoid', '==', geoid).stream()])

    return {'meetings': data}


def update_meeting(app, meeting_id, data):
    # Validate the input JSON data against JSON schema
    validate(instance=data, schema=meeting_schema)

    # Collection for offices
    col_ref = app.officedb.collection('meetings')

    # Update document with office_id with data provided
    col_ref.document(meeting_id).update(data)

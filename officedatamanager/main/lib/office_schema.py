office_schema = {
    "type": "object",
    "properties": {
        "contact.email": {"type": "string"},
        "contact.phone.area": {"type": "string"},
        "contact.phone.country": {"type": "string"},
        "contact.phone.line": {"type": "string"},
        "contact.phone.office": {"type": "string"},
        "filingWindow.end": {"type": "number"},
        "filingWindow.start": {"type": "number"},
        "geoid": {"type": "string"},
        "officeholder.gender": {"type": "string"},
        "officeholder.name.first": {"type": "string"},
        "officeholder.name.last": {"type": "string"},
        "officeholder.socialMedia.facebook": {"type": "string"},
        "officeholder.socialMedia.instagram": {"type": "string"},
        "officeholder.socialMedia.twitter": {"type": "string"},
        "officeholder.termend": {"type": "number"},
        "state": {"type": "string"},
        "term.end": {"type": "number"},
        "term.start": {"type": "number"},
        "timestamp": {"type": "number"},
        "title": {"type": "string"},
        "website": {"type": "string"}
    },
    "additionalProperties": False
}
